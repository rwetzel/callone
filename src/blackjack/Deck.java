package blackjack;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: rwetzel
 * Date: 11/16/13
 * Time: 7:48 PM
 * To change this template use File | Settings | File Templates.
 */
//This class will encapsulate the blackjack deck:
public class Deck {
    //instance field
    private static final int NUM_SHOES = 6;
    private static final int NUM_CARDS_PER_DECK = 52;
    public static final int NUM_CARDS_6_DECKS = 312;
    private Card[] mCards;
    private Random mRandom;
    private int mCardCount;
    private Card mTempCard;

    public Deck(){
        //compose Six decks:
        mRandom = new Random();
        mCards = new Card[NUM_CARDS_6_DECKS];
        resetDeck();
        mCardCount = NUM_CARDS_6_DECKS;

    }//end Constructor

    //This method sets the deck to have all the cards of 6 decks:
    public void resetDeck(){
        //create sixDeckShoe Deck
        int nCount = 0;
        for (int nC = 0; nC < NUM_SHOES; nC++) {
            for (int nS = 0; nS < Card.CARD_SUITS.length; nS++) {
                for (int nF = 0; nF < Card.CARD_FACES.length; nF++) {
                    mCards[nCount] = new Card(Card.CARD_FACES[nF],Card.CARD_SUITS[nS]);
                    nCount ++;
                }//end numFaces
            }//end numSuits
        }//end numShoes
        mCardCount = NUM_CARDS_6_DECKS;
    }//end reset deck

    //This method deals two cards to a player
    public Hand dealHand(){
        Hand hand = new Hand();
        hand.addCardToHand(dealCard());
        hand.addCardToHand(dealCard());
        //return a set of two cards:
        return hand;
    }// end dealPlayer method

    public Card dealCard(){
        //get random number in range of 6 decks (randomness simulates shuffling)
        int nRandCard = mRandom.nextInt(NUM_CARDS_6_DECKS);
        //make sure the random index has not previously been drawn, draw until not null
        while (mCards[nRandCard] == null){
            nRandCard = mRandom.nextInt(NUM_CARDS_6_DECKS);
        }//end while
        //create temp card ref and replace that index with null to indicate that card has been drawn
        Card tmpCard = mCards[nRandCard];
        mCards[nRandCard] = null;
        //decrement total card count and return new card.
        mCardCount--;
        return tmpCard;
    }//end dealCard

    //returns the number of cards in the deck.
    public int getDeckSize(){
        return mCardCount;
    }//end getDeckSize



}//end Deck class
