package blackjack;

/**
 * Created with IntelliJ IDEA.
 * User: rwetzel
 * Date: 11/16/13
 * Time: 6:33 PM
 * To change this template use File | Settings | File Templates.
 */
//This class will simulate a card object for the black jack game
public class Card {
    //instance fields:
    public static final char[] CARD_FACES = {'2','3','4','5','6','7','8','9','T','J','Q','K','A'};
    public static final char[] CARD_SUITS = {'H','D','S','C'};
    public static final int[] CARD_VALUES = {1,2,3,4,5,6,7,8,9,10,11};
    private char mSuit;
    private char mCard;
    private int mValue;

    //constructor
    public Card(char card, char suit){
        mCard = card;
        mSuit = suit;
    }//end Card Constructor

    public char getSuit() {
        return mSuit;
    }//end getSuit

    public void setSuit(char suit) {
        mSuit = suit;
    }//end setSuit

    public char getCard() {
        return mCard;
    }//end getCard

    public void setCard(char card) {
        mCard = card;
    }//end setCard

    //returns numeric value for scoring purposes of a card:
    public int getValue(){
        char nCard = mCard;
        switch (nCard){
            case '2':
                return 2;
            case '3':
                return 3;
            case '4':
                return 4;
            case '5':
                return 5;
            case '6':
                return 6;
            case '7':
                return 7;
            case '8':
                return 8;
            case '9':
                return 9;
            case 'T':
                return 10;
            case 'J':
                return 10;
            case 'Q':
                return 10;
            case 'K':
                return 10;
            case 'A':
                return 11;
            default:
                return 0;
        }//end switch
    }//end getValue

    @Override
    //return string in the format of the card image names:
    public String toString() {
        return mCard + "_of_" + mSuit;
    }
}//end Card class
