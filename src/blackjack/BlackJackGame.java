package blackjack;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.awt.Color;


/**
 * Created with IntelliJ IDEA.
 * User: rwetzel
 * Date: 11/17/13
 * Time: 3:16 AM
 * To change this template use File | Settings | File Templates.
 */
public class BlackJackGame {

    //panel instance fields:
    private JPanel mPanel;
    private JPanel mPanelEast;
    private JPanel mPanelWest;
    private JPanel mPanelSouth;
    private JPanel mPanelNorth;
    private JButton mNEWGAMEButton;
    private JButton mDealButton;
    private JButton mHitButton;
    private JButton mStandButton;
    private JButton mDoubleDownButton;
    private JButton mSurrenderButton;
    private JButton mBet$5Button;
    private JButton mBet$25Button;
    private JTextField mBetTextField;
    private JTextField mMoneyTextField;
    private JTextField mHandTextField;
    private JTextArea mTickerTextArea;

    //my instance fields:
    private static final int FRAME_WIDTH = 700;
    private static final int FRAME_HEIGHT = 700;
    private static final String STR_BET_25 = "Bet $25";
    private static final String STR_BET_5 = "Bet $5";
    private static final double D_BET_25 = 25.0;
    private static final double D_BET_5 = 5.0;
    private static final int BLACK_JACK = 21;
    private NumberFormat mDollarFormat;
    //if true, means user has doubled down or surrender, respectively, and for revealing dealer's cards
    private boolean mDD, mSurrender, mHandOver;
    private Deck mDeck;
    private double mCurrentBet;
    private Player mPlayer;
    private Dealer mDealer;


    //buttons:
    private ActionListener mNewGameListen, mDealListen, mHitListen, mStandListen;
    private ActionListener mDDListen, mSurrenderListen, mBetListen;


    //main -- i.e. show BlackJackGame GUI
    public static void main(String[] args) {
        //need to create new Frame of BlackJackGame type --
        JFrame frame = new JFrame("BlackJack Game");
        frame.setContentPane(new BlackJackGame().mPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(FRAME_WIDTH,FRAME_HEIGHT);
        frame.setVisible(true);
    }//end main

    public BlackJackGame(){
        //initialization:
        mDollarFormat = NumberFormat.getCurrencyInstance();
        mDeck = new Deck();
        mCurrentBet = 0.0;
        mPlayer = new Player();
        mDealer = new Dealer();



        //intialize text areas:
        mBetTextField.setText(mDollarFormat.format(mCurrentBet));
        mHandTextField.setText(""+mPlayer.getRoundScore());
        mMoneyTextField.setText(mDollarFormat.format(mPlayer.getMoney()));
        mTickerTextArea.setText("Place Bet! Click 'Deal' button to begin!\n");


        //add actionlisteners to buttons:
        mNewGameListen = new NewGameListener();
        mNEWGAMEButton.addActionListener(mNewGameListen);
        mBetListen = new BetListener();
        mBet$5Button.addActionListener(mBetListen);
        mBet$25Button.addActionListener(mBetListen);
        mDealListen = new DealListener();
        mDealButton.addActionListener(mDealListen);
        mHitListen = new HitListener();
        mHitButton.addActionListener(mHitListen);
        mStandListen = new StandListener();
        mStandButton.addActionListener(mStandListen);
        mDDListen = new DDListener();
        mDoubleDownButton.addActionListener(mDDListen);
        mSurrenderListen = new SurrenderListener();
        mSurrenderButton.addActionListener(mSurrenderListen);

    }//end constructor

    //draws all cards for both user AND dealer:
    //pass in the graphics context of the jPanel you want to draw on
    protected void drawCards(Graphics g, ArrayList<Card> userHand, ArrayList dealerHand) {
        g.setColor(Color.GREEN);
        g.fillRect(0,0,mPanelNorth.getWidth(),mPanelNorth.getHeight());

        BufferedImage bufferedImage;

        //draw users cards
        int nC = 10;
        for (int nII = 0; nII < userHand.size(); nII++) {
            bufferedImage = SoundImageUtils.genBuffImage("/src/blackjack/imgs/" + userHand.get(nII) + ".png");
            //nc = x-pos, 100 = y-pos
            g.drawImage(bufferedImage, nC, 150, 75, 100, null);
            nC = nC +25;
        }//end for

        //draw dealer cards
        //use this counter to position the cards horizontally
        int nC2 = 10;
        for (int nII = 0; nII < dealerHand.size(); nII++) {
            //show first card as the back unless the hand is over
            if (nII == 0 && mHandOver == false){
                bufferedImage = SoundImageUtils.genBuffImage("/src/blackjack/imgs/back.png");
                //nc = x-pos, 100 = y-pos
                g.drawImage(bufferedImage, nC2, 10, 75, 100, null);
                nC2 = nC2 +25;
            }//end if
            else{
                bufferedImage = SoundImageUtils.genBuffImage("/src/blackjack/imgs/" + dealerHand.get(nII) + ".png");
                //nc = x-pos, 100 = y-pos
                g.drawImage(bufferedImage, nC2, 10, 75, 100, null);
                nC2 = nC2 +25;
            }//end else
        }//end for
    }// end drawGraphics method


    //this method should definitely be called at the end of several user buttons
    public void dealerTurn(){
        //if less than half of cards remain then reset the deck:
        if (mDeck.getDeckSize() < Deck.NUM_CARDS_6_DECKS/2){
            mDeck.resetDeck();
        }//end check of deck size
        //dealer needs to keep acquiring cards in hand until score is > 17 OR
        //if score > 21 bust.
        if(mDealer.getRoundScore() > 21){
            mTickerTextArea.append("DEALER BUST! YOU WIN!!\n");
            playerWins();
            return;
        }//end if > 21
        else if(mDealer.getRoundScore() > 17){
            mTickerTextArea.append("Dealer Stands.\n");
            //compareScores
            compareScores();
            return;
        }//end else > 17

        //for now just do:
        else{
            mDealer.getHand().addCardToHand(mDeck.dealCard());
            //mDealerRoundScore += mDealerHand.get(mDealerHand.size()-1).getValue();
            mDealer.setRoundScore(mDealer.getHand().roundScore());
            //display new Card
            drawCards(mPanelNorth.getGraphics(),mPlayer.getHand().getCards(),mDealer.getHand().getCards());
            //recursive call to dealerTurn --> will repeat until score is not less than 17
            dealerTurn();
        }//end else

    }//end dealerTurn

    //will be executed when player wins the hand:
    public void playerWins(){
        //if it was a double down
        if (mDD){
            //return the user amount bet times 1.5:
            //add mCurrentBet twice due to how it is originally subtracted upon betting
            //only add 0.5 of first bet b/c it was a double down:
            mPlayer.setMoney(mPlayer.getMoney() + 0.5 * mCurrentBet + mCurrentBet);
            //print winnings
            mTickerTextArea.append("You won " + mDollarFormat.format(mCurrentBet * 1.5) + "!\n");
        }//end if mDD
        else {
            //return exact amount bet:
            //add mCurrentBet twice due to how it is originally subtracted upon betting
            mPlayer.setMoney(mPlayer.getMoney() + mCurrentBet + mCurrentBet);
            //print winnings
            mTickerTextArea.append("You won " + mDollarFormat.format(mCurrentBet) + "!\n");
        }//end else

        mMoneyTextField.setText(mDollarFormat.format(mPlayer.getMoney()));
        //now end hand:
        handOver();
    }//end playerWins

    //compares scores of both hands and contents in the case of a tie. returns a string indicating the results:
    public void compareScores(){
        //black jack wins over non black jack if tie.
        //return "Win" if player beats dealer
        //return "Lose" if dealer beats player
        //return "push" if hands are equal
        if (mPlayer.getRoundScore() > BLACK_JACK){
            //Should not be possible but dealer wins:
            mTickerTextArea.append("Dealer wins:\n" + mDealer.getRoundScore() + "-" + mPlayer.getRoundScore() +"\n");
            handOver();
        }//end if
        else if (mDealer.getRoundScore() > BLACK_JACK){
            //Should not be possible but user wins:
            mTickerTextArea.append("You win:\n" + mPlayer.getRoundScore() + "-" + mDealer.getRoundScore() +"\n");
            playerWins();
        }//end else-if
        //now compare scores directly:
        else if (mDealer.getRoundScore() > mPlayer.getRoundScore()){
            mTickerTextArea.append("Dealer wins:\n" + mDealer.getRoundScore() + "-" + mPlayer.getRoundScore() +"\n");
            handOver();
        }//end else-if
        else if (mPlayer.getRoundScore() > mDealer.getRoundScore()){
            mTickerTextArea.append("You win:\n" + mPlayer.getRoundScore() + "-" + mDealer.getRoundScore() +"\n");
            playerWins();
        }//end else-if
        else if (mPlayer.getRoundScore() == BLACK_JACK && mPlayer.getRoundScore() == mDealer.getRoundScore()){
            //check to see if either has black jack:
            if (mPlayer.getHand().hasBlackJack()){
                if (!mDealer.getHand().hasBlackJack()){
                    mTickerTextArea.append("You win with black jack!\n" + mPlayer.getRoundScore() +
                            "-" + mDealer.getRoundScore() +"\n");
                    playerWins();
                }//end if
                else{
                    mTickerTextArea.append("PUSH! Both players had blackjack!\n" +
                            "Score was:\n" + mPlayer.getRoundScore() + "-" + mDealer.getRoundScore() +"\n");
                    //return bet to user:
                    mPlayer.setMoney(mPlayer.getMoney() + mCurrentBet);
                    mMoneyTextField.setText(mDollarFormat.format(mPlayer.getMoney()));
                    mTickerTextArea.append("Bet returned...\n");
                    handOver();
                }
            }//end outer-if
            //if dealer has black jack and method gets here then user does NOT have black jack:
            else if(mDealer.getHand().hasBlackJack()){
                mTickerTextArea.append("Dealer wins with black jack!\n" + mDealer.getRoundScore() +
                        "-" + mPlayer.getRoundScore() +"\n");
                handOver();
            }// end else if
            else{ //if both hands have 21 but neither have black jack
                mTickerTextArea.append("PUSH! Score was:\n" + mPlayer.getRoundScore() + "-" + mDealer.getRoundScore() +"\n");
                //return bet to user:
                mPlayer.setMoney(mPlayer.getMoney() + mCurrentBet);
                mMoneyTextField.setText(mDollarFormat.format(mPlayer.getMoney()));
                mTickerTextArea.append("Bet returned...\n");
                handOver();
            }
        } //end else if tied at 21
        else if (mPlayer.getRoundScore() == mDealer.getRoundScore()){

            mTickerTextArea.append("PUSH! Score was:\n" + mPlayer.getRoundScore() + "-" + mDealer.getRoundScore() +"\n");
            //return bet to user:
            mPlayer.setMoney(mPlayer.getMoney() + mCurrentBet);
            mMoneyTextField.setText(mDollarFormat.format(mPlayer.getMoney()));
            mTickerTextArea.append("Bet returned...\n");
            handOver();
        }//end else if tied

    }//end compareScores


    //resets all values at the end of a hand
    public void handOver(){
        //show cards for final time:
        //hand is over so set to true:
        mHandOver = true;
        drawCards(mPanelNorth.getGraphics(),mPlayer.getHand().getCards(),mDealer.getHand().getCards());
        //now end hand:
        if (mSurrender){
            //return half of user's bet upon surrender:
            mPlayer.setMoney(mPlayer.getMoney() + mCurrentBet/2);
            mMoneyTextField.setText(mDollarFormat.format(mPlayer.getMoney()));
            mTickerTextArea.append("Returned half your bet!\n");
        }//end if surrendered

        mPlayer.personHandReset();
        mDealer.personHandReset();
        mCurrentBet = 0;
        mBetTextField.setText(mDollarFormat.format(mCurrentBet));
        mHandTextField.setText(""+mPlayer.getRoundScore());
        mMoneyTextField.setText(mDollarFormat.format(mPlayer.getMoney()));
        mTickerTextArea.append("Place Bet! Click 'Deal' to start next hand!\n");
        mDD = false;
        mSurrender = false;
        mHandOver = false;
    }//end handOver


    //------------------------------BUTTON CLASS METHODS-------------------------------------
    //The Following classes are implementations of button clicks and are implemented in this form.java
    //file as subclasses for readability.

    //add NewGameListener Class:
    class NewGameListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            //need to set deck to initial conditions as well as user and dealer scores:
            mDeck.resetDeck();
            //mUser.resetPlayer();
            //mDealer.resetDealer();

            //reset all data for players in game:
            mSurrender = false;
            mDD = false;

            mPlayer.resetPerson();
            mDealer.resetPerson();
            mCurrentBet = 0;
            mBetTextField.setText(mDollarFormat.format(mCurrentBet));
            mHandTextField.setText(""+mPlayer.getRoundScore());
            mMoneyTextField.setText(mDollarFormat.format(mPlayer.getMoney()));
            mTickerTextArea.setText("Started NEW GAME!\n");
            mTickerTextArea.append("Place Bet! Click 'Deal' button to begin!\n");

            //GOING TO NEED TO CLEAR PANEL WITH CARDS ON IT!!

            mPanelNorth.getGraphics().setColor(Color.GREEN);
            mPanelNorth.getGraphics().fillRect(0, 0, mPanelNorth.getWidth(), mPanelNorth.getHeight());

        }//end actionPerformed
    }//end NewGameListener


    //add DealListener Class:
    class DealListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            //needs to deal two cards in to the users hand then display them face up.
            //then deal two cards in to the dealers hand then display one face down and the second face up.
            //show message to ticker: "select your next move"

            //can't deal unless hands are empty:
            if(!(mPlayer.getHand().getCards().isEmpty())){
                mTickerTextArea.append("Can't deal until hand is over!!\n");
                return;
            }//check to make sure dealing is right option

            //if less than half of cards remain then reset the deck:
            if (mDeck.getDeckSize() < Deck.NUM_CARDS_6_DECKS/2){
                mDeck.resetDeck();
            }//end check of deck size

            //update current score:
            mPlayer.setHand(mDeck.dealHand());
            //now update mUserRoundScore
            mPlayer.setRoundScore(mPlayer.getRoundScore());
            mHandTextField.setText(""+mPlayer.getRoundScore());

            //do same for dealer without displaying score:
            mDealer.setHand(mDeck.dealHand());
            mDealer.setRoundScore(mDealer.getRoundScore());

            //now show the cards on the game surface:
            drawCards(mPanelNorth.getGraphics(),mPlayer.getHand().getCards(),mDealer.getHand().getCards());

            //clarify action on ticker.
            mTickerTextArea.setText("Deal Completed.\n Pick your play from buttons on right: \n");

        }//end actionPerformed
    }//end HitListener

    //add HitListener Class:
    class HitListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            //if haven't been dealt yet then you cant do this method:
            if (mPlayer.getHand().getCards().isEmpty()){
                mTickerTextArea.append("Can't Hit before deal!!\n");
                return;
            }//end check

            //make sure more than half the deck remains:
            if (mDeck.getDeckSize() < Deck.NUM_CARDS_6_DECKS/2){
                mDeck.resetDeck();
            }//end check of deck size
            //do mUserHand .add()
            //add Card returned from deck.dealCard
            //don't need to implement for dealer --> will be taken care of in dealerTurn method
            mPlayer.addCardToHand(mDeck.dealCard());
            mPlayer.setRoundScore(mPlayer.getRoundScore());
            mHandTextField.setText(""+mPlayer.getRoundScore());
            drawCards(mPanelNorth.getGraphics(),mPlayer.getHand().getCards(),mDealer.getHand().getCards());
            mTickerTextArea.append("Hit. \n");

            //Check for bust at end of hit:
            if (mPlayer.getRoundScore() > BLACK_JACK){
                mTickerTextArea.append("SORRY. YOU BUSTED!\nYou lost " +
                        mDollarFormat.format(mCurrentBet) + "... \n");
                //clear all single round data.
                handOver();

            }//end if (bust)
            else {
                mTickerTextArea.append("Now what? \n");
            }//end else

        }//end actionPerformed
    }//end HitListener

    //add StandListener Class:
    class StandListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            //This action effectively ends a user's turn:
            //this means call the computer's turn.
            mTickerTextArea.append("OK! Your turn is over...\n");
            dealerTurn();
        }//end actionPerformed
    }//end StandListener

    //add DDListener Class:
    class DDListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            //signs contract to end turn after one more card:
            //in addition this DOUBLES the bet
            //must have two and ONLY two cards in hand to implement
            if (!(mPlayer.getHand().getCards().size() == 2)){
                mTickerTextArea.append("Can't double down now!!\n");
                return;
            }//end check for plausibility

            //subtract current bet from money again, double bet, set mDD to true
            mPlayer.setMoney(mPlayer.getMoney() - mCurrentBet);
            mCurrentBet = mCurrentBet + mCurrentBet;
            mDD = true;
            mMoneyTextField.setText(mDollarFormat.format(mPlayer.getMoney()));
            mBetTextField.setText(mDollarFormat.format(mCurrentBet));

            //now add single card to users hand:
            mPlayer.addCardToHand(mDeck.dealCard());
            //redraw:
            drawCards(mPanelNorth.getGraphics(),mPlayer.getHand().getCards(),mDealer.getHand().getCards());

            //make sure didn't bust:
            //Check for bust at end of hit:
            if (mPlayer.getRoundScore() > BLACK_JACK){
                mTickerTextArea.append("SORRY. YOU BUSTED!\nYou lost " +
                        mDollarFormat.format(mCurrentBet) + "... \n");
                //clear all single round data.
                handOver();

            }//end if (bust)
            //if didn't bust then it's the dealer's turn:
            else{
                dealerTurn();
            }//end else

        }//end actionPerformed
    }//end DDListener

    //add SurrenderListener Class:
    class SurrenderListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            //signify that user has surrendered:
            mSurrender = true;
            //this ends the hand
            handOver();
        }//end actionPerformed
    }//end SurrenderListener

    //add BetListener Class:
    class BetListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            //if cards have already been dealt then you cannot bet!
            if(!(mPlayer.getHand().getCards().isEmpty())){
                mTickerTextArea.append("Can't bet after deal!\n");
                return;
            }//end card check

            //get text from button to figure out how much to add:
            //update mCurrentBet
            //update relevant text fields.
            String strBetAmt = ((JButton)e.getSource()).getText();
            if (strBetAmt.equalsIgnoreCase(STR_BET_25)){
                mCurrentBet = mCurrentBet + D_BET_25;
                mPlayer.setMoney(mPlayer.getMoney() - D_BET_25);
                mTickerTextArea.append("Added $25.00 Bet\n");
            }//end if
            else if (strBetAmt.equalsIgnoreCase(STR_BET_5)){
                mCurrentBet = mCurrentBet + D_BET_5;
                mPlayer.setMoney(mPlayer.getMoney() - D_BET_5);
                mTickerTextArea.append("Added $5.00 Bet\n");
            }//end else if

            //update relevant textfields:
            mMoneyTextField.setText(mDollarFormat.format(mPlayer.getMoney()));
            mBetTextField.setText(mDollarFormat.format(mCurrentBet));

        }//end actionPerformed
    }//end BetListener




}//end BlackJackGame class
