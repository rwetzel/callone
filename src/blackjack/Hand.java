package blackjack;


import java.util.ArrayList;

//This class will be used to encapsulate a user's hand i.e. the cards and scoring methods.
public class Hand {
    private static final int ACE_VALUE = 11;
    //This class needs a collection to represent hand:
    private ArrayList<Card> mHand;

    //constructor
    public Hand(){
        mHand = new ArrayList<Card>();
    }//end no-arg constructor


    //methods:
    public ArrayList<Card> getCards() {
        return mHand;
    }//end getHand

    public void addCardToHand(Card card){
        mHand.add(card);
    }//end addCardToHand

    //Reset Hand in the event of new round or game:
    public void resetHand(){
        mHand.clear();
    }//end resetHand


    //returns true if the hand contains a blackJack:
    public boolean hasBlackJack(){
        for (Card card : mHand) {
            if (card.getCard() == 'J'){
                if (card.getSuit() == 'S' || card.getSuit() == 'C'){
                    return true;
                }//end if suit
            }//end if card Card
        }//end for
        return false;
    }//end hasBlackJack


    //returns true if the hand contains an Ace:
    public boolean hasAce(){
        for (Card card : mHand) {
            if (card.getCard() == 'A'){
                return true;
            }//end if A
        }//end for
        return false;
    }//end hasAce

    //totals cards in a black jack hand:
    //Calculates so that Aces are 11 UNLESS they would make the hand bust in which case they are
    //treated as 1.
    public int roundScore(){
        int nHandScore = 0;
        ArrayList<Card> aceList = new ArrayList<Card>();
        for (Card card : mHand) {
            if (card.getValue() == ACE_VALUE){
                aceList.add(card);
            }//end if
            else{
                nHandScore += card.getValue();
            }//end else
        }//end for
        if(!aceList.isEmpty()){
            for (int nC = 0; nC < aceList.size(); nC++){
                //to add 11 score must be less than 10 - # aces left in aceList
                if (nHandScore <= 10 - (aceList.size() - 1 - nC)){
                    nHandScore += ACE_VALUE;
                }//end inner if
                else{ //if not room for +11 and still be under then add 1.
                    nHandScore += 1;
                }
            }//end for
        }//end if list empty
        return nHandScore;
    }//end roundScore


}//end Hand Class
