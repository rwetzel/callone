package blackjack;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: rwetzel
 * Date: 11/16/13
 * Time: 7:38 PM
 * To change this template use File | Settings | File Templates.
 */
//This class will encapsulate the data and methods for a non-computer black jack player:
public class Player extends Person {

    //instance fields:
    private double mMoney;
    //private int mRoundScore;
    //private ArrayList<Card> mHand;
    private boolean mIsDealer;

    public Player(){
        super();
        //players should always Start with $1000.00
        mMoney = 1000.00;
        mIsDealer = false;
    }//end constructor

    public double getMoney() {
        return mMoney;
    }//end getMoney


    public void setMoney(double money) {
        mMoney = money;
    }//end setMoney
                             /*
    //resets instance fields to those at time of initial construction
    public void resetPlayer(){
        mHand = new ArrayList<>();
        mRoundScore = 0;
        mMoney = 1000.00;
    }//end resetPlayer
                           */
    //resets instance fields to those at time of initial construction
    @Override
    public void resetPerson(){
        mMoney = 1000.00;
    }//end resetPerson


    public boolean isDealer() {
        return mIsDealer;
    }//isDealer


}//end Player Class
