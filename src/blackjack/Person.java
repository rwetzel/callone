package blackjack;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: rwetzel
 */
//This class will be extended by the player (user) and the Dealer (computer)
public abstract class Person {
    private int mRoundScore;
    private Hand mHand;

    //no arg constructor:
    public Person(){
        mRoundScore = 0;
        mHand = new Hand();
    }//end constructor;

    public Hand getHand() {
        return mHand;
    }//end getHand

    public void setHand(Hand hand) {
        mHand = hand;
    }//end setHand

    public void personHandReset(){
        mHand.resetHand();
        mRoundScore = 0;
    }

    public void addCardToHand(Card card){
        mHand.addCardToHand(card);
    }//end addCardToHand

    public int getRoundScore() {
        return mHand.roundScore();
    }//end getRoundScore

    public void setRoundScore(int roundScore) {
        mRoundScore = roundScore;
    }//end setRoundScore

    //resets instance fields to those at time of initial construction
    public void resetPerson(){
        mHand.resetHand();
        mRoundScore = 0;
    }//end resetPerson

    //returns true if is a dealer/false if just a player:
    public abstract boolean isDealer();


}//end Person
