package blackjack;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: rwetzel
 * Date: 11/16/13
 * Time: 7:47 PM
 * To change this template use File | Settings | File Templates.
 */
//This class will represent the data and methods of the dealer/Computer player:
public class Dealer extends Person {

    //signifies that this person is the dealer
    private boolean mIsDealer;

    public Dealer(){
        super();
        mIsDealer = true;
    }//end no arg constructor

    public void resetPerson(){
        super.resetPerson();
        mIsDealer = true;
    }//end resetPerson


    public boolean isDealer() {
        return mIsDealer;
    }//isDealer

}//end Dealer Class
