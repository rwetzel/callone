
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * RILEY WETZEL --> CallOne Interview FollowUp
 * Created with IntelliJ IDEA.
 * User: rwetzel
 * Date: 11/21/13
 */
public class CallOneMonthToString {

    //main method to prove implementation:
    public static void main(String[] args) {
        //create scanner to read input initialize integer to hold user input.
        Scanner scanner = new Scanner(System.in);
        int nDayNumber;
        //request user input and print result:
        System.out.println("Please enter a day of the month as an integer: ");
        try {
            //try to initialize user variable: if int is out of acceptable range ask for it again else throw exception.
            nDayNumber = scanner.nextInt();
            while (!(nDayNumber > 0 && nDayNumber < 32)){
                System.out.println("Input must be integer between 1-31! Try again: ");
                nDayNumber = scanner.nextInt();
            }//end while
        }//end try block
        catch (InputMismatchException e) {
            System.out.println("Must be an INTEGER between 1-31. Exiting..." );
            return;
        }//end catch

        //print result of method call:
        System.out.println("The day with its proper suffix is: " + daySuffix(nDayNumber));
    }//end main


    //define simple method to return a day of the month with the proper suffix appended to the end:
    public static String daySuffix(int nDay){
        //can use mod operator to quickly find last digit of int..
        //first check for all th's
        if ((nDay >= 4 && nDay <= 20) || (nDay >= 24 && nDay <=30)){
            return nDay + "th";
        }
        else if (nDay % 10 == 1){
            return nDay + "st";
        }
        else if (nDay % 10 == 2){
            return nDay + "nd";
        }
        else{ //nDay % 10 == 3
            return nDay + "rd";
        }
    }//end nDay method

}//end CallOneMonthToString Class